﻿using System.Web.Mvc;
using System.Web.Routing;

namespace e_store.App
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "General",
                url: "usr/{action}",
                defaults: new { controller = "UserPartial", action = "Empty" });

            routes.MapRoute(
                name: "Admin",
                url: "adm/{action}",
                defaults: new { controller = "AdminPartial", action = "Empty" });

            //routes.MapRoute(
            //    name: "Default",
            //    url: "Home/{action}",
            //    defaults: new { controller = "Home", action = "Index" }
            //);

            // url: "{controller}/{action}/{id}",
            // url: "{*catchAll}",

            //routes.MapRoute(
            //    name: "Admin Panel",
            //    url: "Admin/{action}",
            //    defaults: new { controller = "Admin", action = "Index" }
            //);
        }
    }
}
