﻿using System.Web;
using System.Web.Optimization;

namespace e_store.App
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css-style").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/Site.css",
                      "~/Content/my-style.css",
                      "~/Content/loading-bar.css"));

            bundles.Add(new ScriptBundle("~/bundles/angular-lib").Include(
                      "~/Scripts/angular.js",
                      "~/Scripts/angular-route.js",
                      "~/Scripts/angular-animate.js",
                      "~/Scripts/angular-messages.js",
                      "~/Scripts/loading-bar.js",
                      "~/Scripts/ng-upload.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-ctrls").Include(
                      "~/ng-app-user/appUser.js",
                      "~/ng-app-user/controller/home/homeCtrl.js",
                      "~/ng-app-user/controller/about/aboutCtrl.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-admin").Include(
                      "~/ng-app-admin/appAdmin.js",
                      "~/ng-app-admin/dataServiceAdmin.js",
                      "~/ng-app-admin/controller/home/homeCtrl.js",
                      "~/ng-app-admin/controller/category/categoryCtrl.js",
                      "~/ng-app-admin/controller/item/itemCtrl.js",
                      "~/ng-app-admin/controller/item/itemEditCtrl.js",
                      "~/ng-app-admin/controller/subcategory/subcategoryCtrl.js"));
        }
    }
}
