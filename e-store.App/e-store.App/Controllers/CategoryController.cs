﻿using e_com.Commons.Dto;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace e_store.App.Controllers
{
    [RoutePrefix("category")]
    public class CategoryController : Controller
    {
        RestClient restClient = new RestClient(ConfigurationManager.AppSettings["api-url"].ToString());

        [Route("get/{id?}")]
        public ActionResult Get(int? id = null)
        {
            string uri = "api/product/category?id=";
            if (id != null && id > 0)
                uri += id;
            var request = new RestRequest(uri, Method.GET) { RequestFormat = DataFormat.Json };
            var queryResult = restClient.Execute<List<CategoryDto>>(request).Data;
            return Json(queryResult, JsonRequestBehavior.AllowGet);
        }

        [Route("add")]
        [HttpPost]
        public ActionResult Add(CategoryDto dto)
        {
            string uri = "api/product/add/category";
            var request = new RestRequest(uri, Method.POST) { RequestFormat = DataFormat.Json };
            //request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddBody(dto);
            //request.AddParameter("application/json", dto, ParameterType.GetOrPost);

            var response = restClient.Execute<CategoryDto>(request);
            return Json(response.Content.ToString());
        }

    }
}