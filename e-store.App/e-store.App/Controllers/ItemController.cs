﻿using e_com.Commons.Dto;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace e_store.App.Controllers
{
    [RoutePrefix("item")]
    public class ItemController : Controller
    {

        RestClient restClient = new RestClient(ConfigurationManager.AppSettings["api-url"].ToString());

        [Route("add")]
        [HttpPost]
        public ActionResult Add(ItemDto dto)
        {
            string uri = "api/product/add/item";
            var request = new RestRequest(uri, Method.POST) { RequestFormat = DataFormat.Json };
            request.AddBody(dto);

            var response = restClient.Execute<ItemDto>(request);
            return Json(response.Content.ToString());
        }

        [Route("itemStatus")]
        [HttpGet]
        public ActionResult GetItemStatus()
        {
            string uri = "api/product/itemStatus";
            var request = new RestRequest(uri, Method.GET) { RequestFormat = DataFormat.Json };
            var queryResult = restClient.Execute<List<ItemStatusDto>>(request).Data;
            return Json(queryResult, JsonRequestBehavior.AllowGet);
        }
    }
}