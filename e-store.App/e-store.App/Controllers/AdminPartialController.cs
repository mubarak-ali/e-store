﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace e_store.App.Controllers
{
    public class AdminPartialController : Controller
    {
        public ActionResult home()
        {
            return View("home/home");
        }

        public ActionResult category()
        {
            return View("category/category");
        }

        public ActionResult subcategory()
        {
            return View("subcategory/subcategory");
        }

        public ActionResult item()
        {
            return View("item/item");
        }
    }
}