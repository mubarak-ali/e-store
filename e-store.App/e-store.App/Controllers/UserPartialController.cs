﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace e_store.App.Controllers
{
    public class UserPartialController : Controller
    {
        public ActionResult home()
        {
            return View("home/home");
        }

        public ActionResult about()
        {
            return View("about/about");
        }

    }
}