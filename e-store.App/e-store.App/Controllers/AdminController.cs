﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace e_store.App.Controllers
{
    [RoutePrefix("admin")]
    public class AdminController : Controller
    {
        [Route("")]
        public ActionResult Index()
        {
            return View();
        }
    }
}