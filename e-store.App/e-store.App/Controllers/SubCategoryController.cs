﻿using e_com.Commons.Dto;
using RestSharp;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;

namespace e_store.App.Controllers
{
    [RoutePrefix("subcategory")]
    public class SubCategoryController : Controller
    {
        RestClient restClient = new RestClient(ConfigurationManager.AppSettings["api-url"].ToString());

        [Route("get/{id?}")]
        [HttpGet]
        public ActionResult Get(int? id)
        {
            string uri = "api/product/subcategory?id=";
            if (id != null && id > 0)
                uri += id;
            var request = new RestRequest(uri, Method.GET) { RequestFormat = DataFormat.Json };
            var queryResult = restClient.Execute<List<SubCategoryDto>>(request).Data;
            return Json(queryResult, JsonRequestBehavior.AllowGet);
        }

        [Route("add")]
        [HttpPost]
        public ActionResult Add(SubCategoryDto dto)
        {
            string uri = "api/product/add/subcategory";
            var request = new RestRequest(uri, Method.POST) { RequestFormat = DataFormat.Json };
            request.AddBody(dto);

            var response = restClient.Execute<SubCategoryDto>(request);
            return Json(response.Content.ToString());
        }

    }
}