﻿(function () {

    var injectParams = [];

    var homeCtrl = function () {

        var vm = this;
        vm.text = "Welcome to the jungle!";
    };

    homeCtrl.$inject = injectParams;
    angular.module('appUser').controller('homeCtrl', homeCtrl);
}());