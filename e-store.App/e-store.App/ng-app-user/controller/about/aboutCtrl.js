﻿(function () {

    var injectParams = [];

    var aboutCtrl = function () {

        var vm = this;
        vm.text = "This is partial about view!";
    };

    aboutCtrl.$inject = injectParams;
    angular.module('appUser').controller('aboutCtrl', aboutCtrl);
}());