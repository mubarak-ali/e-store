﻿(function () {

    var app = angular.module('appUser', ['ngRoute', 'angular-loading-bar', 'ngAnimate']);

    app.config(['$routeProvider', '$locationProvider', 'cfpLoadingBarProvider',
        function ($routeProvider, $locationProvider, cfpLoadingBarProvider) {

            $routeProvider
                .when('/home', {
                    title: 'Home Page',
                    templateUrl: 'usr/home',
                    controller: 'homeCtrl',
                    controllerAs: 'vm'
                })
                .when('/about', {
                    title: 'About My Store',
                    templateUrl: 'usr/about',
                    controller: 'aboutCtrl',
                    controllerAs: 'vm'
                })
                .otherwise({
                    redirectTo: '/home'
                });

            $locationProvider.html5Mode(true);

            cfpLoadingBarProvider.includeSpinner = false;
            cfpLoadingBarProvider.latencyThreshold = 100;

        }]);

    app.run(['$location', '$rootScope', '$templateCache', function ($location, $rootScope, $templateCache) {

        $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {  //using success callback of route change
            if (current.$$route && current.$$route.title) { //Checking whether $$route is initialised or not
                $rootScope.title = current.$$route.title;
            }
        });

        $rootScope.$on('$routeChangeStart', function (event, next, current) {
            if (typeof (current) !== 'undefined') {
                $templateCache.remove(current.templateUrl);
            }
        });
    }]);

}());