﻿(function () {

    var injectParams = ["dataServiceAdmin"];

    var itemCtrl = function (dataServiceAdmin) {

        var vm = this;
        vm.id = 0;
        vm.obj = {};
        vm.categoryId = -1;
        vm.subCategoryId = -1;
        vm.type = 0;
        init();

        function init() {

            dataServiceAdmin.getCategory().then(function (results) {
                vm.categories = results.data;
            }, function (error) {
                console.log("Error in fetching categories!! Error => " + error);
            });

            dataServiceAdmin.getItemStatus().then(function (results) {
                vm.itemStatus = results;
            }, function (error) {
                console.log("Error in fetching categories!! Error => " + error);
            });
            
        };

        vm.changeCategory = function () {
            if (vm.categoryId != -1) {
                dataServiceAdmin.getSubCategory(vm.categoryId).then(function (results) {
                    vm.subCategories = results;
                }, function (error) {
                    console.log("Error in fetching sub-categories!! Error => " + error);
                });
            }
            else {
                console.log("category is not correct! Id => " + vm.categoryId);
            }
        };

        vm.saveItem = function () {
            vm.obj.categoryId = vm.categoryId;
            vm.obj.subCategoryId = vm.subCategoryId;
            vm.obj.itemStatusId = vm.itemStatusId;
            dataServiceAdmin.saveItem(vm.obj).then(function (results) {
                console.log("Inserted Id => " + results.data);
                vm.obj.description = "";
                vm.obj.name = "";
                vm.obj.code = "";
            }, function (error) {
                console.log("Error saving. Error => " + error);
            });
        }
    };

    itemCtrl.$inject = injectParams;
    angular.module('appAdmin').controller('itemCtrl', itemCtrl);
}());