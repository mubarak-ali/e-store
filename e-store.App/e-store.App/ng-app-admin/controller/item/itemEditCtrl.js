﻿(function () {

    var injectParams = ["$location", "$routeParams", "dataServiceAdmin"];

    var itemEditCtrl = function ($location, $routeParams, dataServiceAdmin) {

        var vm = this;
        vm.id = 0;
        vm.obj = {};
        vm.categoryId = -1;
        vm.subCategoryId = -1;
        vm.type = ($routeParams.addItem) ? parseInt($routeParams.addItem) : $location.path('/item');
        init();

        function init() {


        };

        function loadDdl() {
            dataServiceAdmin.getCategory().then(function (results) {
                vm.categories = results.data;
            }, function (error) {
                console.log("Error in fetching categories!! Error => " + error);
            });

        };

        vm.changeCategory = function () {
            if (vm.categoryId != -1) {
                dataServiceAdmin.getSubCategory(vm.categoryId).then(function (results) {
                    vm.subCategories = results;
                }, function (error) {
                    console.log("Error in fetching sub-categories!! Error => " + error);
                });
            }
            else {
                console.log("category is not correct! Id => " + vm.categoryId);
            }
        };
    };

    itemEditCtrl.$inject = injectParams;
    angular.module('appAdmin').controller('itemEditCtrl', itemEditCtrl);
}());