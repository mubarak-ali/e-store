﻿(function () {

    var injectParams = ["dataServiceAdmin"];

    var subcategoryCtrl = function (dataServiceAdmin) {

        var vm = this;
        vm.id = 0;
        vm.obj = {};
        init();

        function init() {
            dataServiceAdmin.getCategory(0).then(function (results) {
                vm.categories = results.data;
            }, function (error) {
                console.log("Error in fetching categories!! Error => " + error);
            });
        }

        vm.saveSubCategory = function () {
            vm.obj.CategoryId = vm.categoryId;
            dataServiceAdmin.saveSubCategory(vm.obj).then(function (results) {
                console.log("Inserted Id => " + results.data);
                vm.obj.description = "";
                vm.obj.name = "";
                vm.obj.code = "";
            }, function (error) {
                console.log("Error saving. Error => " + error);
            });
        }
    };

    subcategoryCtrl.$inject = injectParams;
    angular.module('appAdmin').controller('subcategoryCtrl', subcategoryCtrl);
}());