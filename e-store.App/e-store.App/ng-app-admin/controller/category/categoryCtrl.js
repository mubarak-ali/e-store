﻿(function () {

    var injectParams = ["dataServiceAdmin"];

    var categoryCtrl = function (dataServiceAdmin) {

        var vm = this;
        vm.obj = {};
        vm.categories = [];
        init();

        function init() {
            vm.showAdd = false;
            dataServiceAdmin.getCategory().then(function (results) {
                console.log("List size => " + results.data.length);
                vm.categories = results.data;
            }, function (error) {
                console.log("Error occurred. Error => " + error);
            });
        };

        vm.saveCategory = function () {
            dataServiceAdmin.saveCategory(vm.obj).then(function (results) {
                console.log("Id => " + results.data);
                vm.obj.description = "";
                vm.obj.name = "";
                init();
            }, function (error) {
                console.log("Error during save! Error => " + error);
            });
        };

        vm.editCategory = function (id) {
            dataServiceAdmin.getCategory(id).then(function (results) {
                console.log("Length => " + results.data.length);
                vm.obj.id = results.data[0].Id;
                vm.obj.name = results.data[0].Name;
                vm.obj.description = results.data[0].Description;
                vm.showAdd = true;
            },
            function (error) {
                console.log("Error occurred. Error => " + error);
            });
        };
    };

    categoryCtrl.$inject = injectParams;
    angular.module('appAdmin').controller('categoryCtrl', categoryCtrl);
}());