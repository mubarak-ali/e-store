﻿(function () {

    var app = angular.module('appAdmin', ['ngRoute', 'angular-loading-bar', 'ngAnimate', 'ngMessages']);

    app.config(['$routeProvider', '$locationProvider', 'cfpLoadingBarProvider',
        function ($routeProvider, $locationProvider, cfpLoadingBarProvider) {

            $routeProvider
                .when('/', {
                    title: 'Home Page',
                    templateUrl: '/adm/home',
                    controller: 'homeCtrl',
                    controllerAs: 'vm'
                })
                .when('/category', {
                    title: 'Category Page',
                    templateUrl: '/adm/category',
                    controller: 'categoryCtrl',
                    controllerAs: 'vm'
                })
                .when('/subcategory', {
                    title: 'Sub-Category Page',
                    templateUrl: '/adm/subcategory',
                    controller: 'subcategoryCtrl',
                    controllerAs: 'vm'
                })
                .when('/item', {
                    title: 'Item Page',
                    templateUrl: '/adm/item',
                    controller: 'itemCtrl',
                    controllerAs: 'vm'
                })
                //.when('/item/:addItem?', {
                //    title: 'Item Page',
                //    templateUrl: '/adm/item',
                //    controller: 'itemEditCtrl',
                //    controllerAs: 'vm'
                //})
                .otherwise({
                    redirectTo: '/'
                });

            cfpLoadingBarProvider.includeSpinner = false;
            cfpLoadingBarProvider.latencyThreshold = 100;

        }]);

    app.run(['$location', '$rootScope', '$templateCache', function ($location, $rootScope, $templateCache) {

        $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {  //using success callback of route change
            if (current.$$route && current.$$route.title) { //Checking whether $$route is initialised or not
                $rootScope.title = current.$$route.title;
            }
        });

        //$rootScope.$on('$routeChangeStart', function (event, next, current) {
        //    if (typeof (current) !== 'undefined') {
        //        $templateCache.remove(current.templateUrl);
        //    }
        //});
    }]);

}());