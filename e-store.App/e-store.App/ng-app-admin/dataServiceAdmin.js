﻿(function () {

    var injectParams = ["$http", "$location"];

    var dataServiceAdmin = function ($http, $location) {

        var factory = {};

        factory.getCategory = function (id) {
            var pkid = id == undefined ? 0 : id;
            return $http({
                method: 'GET',
                url: '/category/get?id=' + pkid,
                cache: false
            }).success(function (results) {
                console.log('category received!');
                return results.data;
            }).error(function (results) {
                console.log('Error!!');
                return null;
            });
        };

        factory.getSubCategory = function (id) {
            return $http.get('/subcategory/get?id=' + id, { cache: false }).then(
                    function (results) {
                        console.log('subcategory received!');
                        return results.data;
                    },
                    function (error) {
                        console.log('Error subcategory get. Error: ' + error);
                    }
            )
        };

        factory.getItemStatus = function () {
            return $http.get('/item/itemStatus/', { cache: false }).then(
                    function (results) {
                        console.log('item status received!');
                        return results.data;
                    },
                    function (error) {
                        console.log('Error subcategory get. Error: ' + error);
                    }
            )
        };

        factory.saveCategory = function (obj) {
            return $http({
                method: 'POST',
                url: '/category/add',
                // data: JSON.stringify({ id: obj.id, maquee: obj.marquee })
                data: obj,
                cache: false
            }).success(function (data, status, headers, config) {
                console.log(status);
                return data;
            }).error(function (data, status, headers, config) {
                console.log(status);
                return 'Unexpected Error';
            });
        };

        factory.saveSubCategory = function (obj) {
            return $http({
                method: 'POST',
                url: '/subcategory/add',
                data: obj,
                cache: false
            }).success(function (data, status, headers, config) {
                console.log(status);
                return data;
            }).error(function (data, status, headers, config) {
                console.log(status);
                return 'Unexpected Error';
            });
        };

        factory.saveItem = function (obj) {
            return $http.post('/item/add',{ dto : obj }, { cache: false }).then(
                    function (results) {
                        console.log('subcategory added!');
                        return results.data;
                    },
                    function (error) {
                        console.log('Error subcategory add. Error: ' + error);
                    }
            )
        };

        return factory;
    };

    dataServiceAdmin.$inject = injectParams;
    angular.module('appAdmin').factory('dataServiceAdmin', dataServiceAdmin);

}());