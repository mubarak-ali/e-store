﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace e_store.App.ViewModels
{
    public class CategoryVM
    {

        public int Id { get; set; }

        public string Description { get; set; }

        [Required]
        [MaxLength(255, ErrorMessage = "Cannot exceed more 255 characters")]
        public string Name { get; set; }

    }
}