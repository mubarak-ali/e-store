﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Routing;
using System.Linq;
using System.Text;

namespace e_store.App.Helpers
{
    public static class MyMvcHtml
    {

        public static MvcHtmlString AngularTextBoxFor
            (this HtmlHelper htmlHelper,
            string modelName,
            object htmlAttributes = null,
            bool required = false,
            bool readOnly = false)
        {
            var builder = new TagBuilder("input");

            #region The default setup

            var dict = new RouteValueDictionary(htmlAttributes);
            string clazz = string.Empty;
            string maxLength = string.Empty;
            string type = string.Empty;

            foreach (var item in dict)
            {
                if (item.Key == "class")
                    clazz = item.Value.ToString();
                if (item.Key == "maxlength")
                    maxLength = item.Value.ToString();
                if (item.Key == "type")
                    type = item.Value.ToString();
            }//foreach

            if (string.IsNullOrEmpty(clazz))
                clazz = "form-control";
            if (string.IsNullOrEmpty(type))
                type = "text";

            #endregion

            //Create the Id attribute.
            builder.GenerateId(modelName.Replace('.', '_'));

            //Adding the other attributes.
            builder.MergeAttribute("type", type);
            builder.MergeAttribute("data-ng-model", modelName);
            builder.MergeAttribute("class", clazz);

            if (!string.IsNullOrEmpty(maxLength))
                builder.MergeAttribute("maxlength", maxLength);
            if (required == true)
                builder.MergeAttribute("ng-required", string.Empty);
            if (readOnly == true)
                builder.MergeAttribute("readonly", string.Empty);

            string html = builder.ToString(TagRenderMode.SelfClosing);
            return MvcHtmlString.Create(html);

        }//AngularTextBoxFor

        #region Previous version of the extension (for reference)
        //public static MvcHtmlString AngularTextBoxFor
        //    (this HtmlHelper htmlHelper,  
        //    string modelName,
        //    object htmlAttributes = null,
        //    bool required = false,
        //    bool readOnly = false)
        //{
        //    var dict = new RouteValueDictionary(htmlAttributes);
        //    string clazz = string.Empty;
        //    string maxLength = string.Empty;
        //    string type = string.Empty;

        //    foreach (var item in dict)
        //    {
        //        if (item.Key == "class")
        //            clazz = item.Value.ToString();
        //        if (item.Key == "maxlength")
        //            maxLength = item.Value.ToString();
        //        if (item.Key == "type")
        //            type = item.Value.ToString();
        //    }//foreach

        //    //set the default class if no class attribute is provided.
        //    if (string.IsNullOrEmpty(clazz))
        //        clazz = "form-control";

        //    //set the default type to text if no type attribute is provided.
        //    if (string.IsNullOrEmpty(type))
        //        type = "text";

        //    var wrap = $"<input type='{type}' id='{modelName.Replace('.', '_')}' data-ng-model='{modelName}' class='{clazz}'";
        //    if (readOnly == true)
        //        wrap += " readonly";
        //    if(required == true)
        //        wrap += " ng-required";
        //    if (maxLength != string.Empty)
        //        wrap += $" maxlength='{maxLength}' ";
        //    wrap += ">";

        //    return new MvcHtmlString(wrap);
        //}//AngularTextBoxFor

        #endregion

        public static MvcHtmlString AngularDropDownFor
            (this HtmlHelper htmlHelper,
            string modelName,
            string dataList,
            object htmlAttributes = null,
            bool required = false)
        {
            var builder = new TagBuilder("select");

            #region The default values setup.

            var dict = new RouteValueDictionary(htmlAttributes);
            string clazz = string.Empty;
            string key = string.Empty;
            string value = string.Empty;
            string onChange = string.Empty;

            foreach (var item in dict)
            {
                if (item.Key == "class")
                    clazz = item.Value.ToString();
                if (item.Key == "key")
                    key = item.Value.ToString();
                if (item.Key == "value")
                    value = item.Value.ToString();
                if (item.Key == "onChange")
                    onChange = item.Value.ToString();
            }//foreach

            //set the default class if no class attribute is provided.
            if (string.IsNullOrEmpty(clazz))
                clazz = "form-control";
            #endregion

            //Create the Id attribute.
            builder.MergeAttribute("class", clazz);
            builder.MergeAttribute("data-ng-model", modelName);
            builder.MergeAttribute("ng-options", string.Format("d.{0} as d.{1} for d in {2}", key, value, dataList));

            if (!string.IsNullOrEmpty(onChange))
                builder.MergeAttribute("ng-change", onChange);
            if (required == true)
                builder.MergeAttribute("ng-required", string.Empty);

            string html = builder.ToString(TagRenderMode.Normal);
            
            return new MvcHtmlString(html);
        }//AngularDropDownFor

        #region Testing for View Model

        public static MvcHtmlString AngularFieldFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper, 
            Expression<Func<TModel, TProperty>> expression, 
            object htmlAttributes = null)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, new ViewDataDictionary<TModel>());
            var name = ExpressionHelper.GetExpressionText(expression);
            var validations = ModelValidatorProviders.Providers.GetValidators(
                    metadata ?? ModelMetadata.FromStringExpression(name, new ViewDataDictionary()),
                    new ControllerContext()).SelectMany(v => v.GetClientValidationRules()).ToArray();

            //var validatorMessages = validations.ToDictionary(k => k.ValidationType, v => v.ErrorMessage);

            string result = "";

            result += GetValidatorDirectivesString(validations);

            return new MvcHtmlString(result);
        }

        private static string GetValidatorDirectivesString(IEnumerable<ModelClientValidationRule> validations)
        {
            var result = "";
            foreach (ModelClientValidationRule val in validations)
            {
                result += " " + ConvertMvcClientValidatorToAngularValidatorString(val);
            }
            return result;
        }

        private static string ConvertMvcClientValidatorToAngularValidatorString(ModelClientValidationRule val)
        {
            switch (val.ValidationType.ToLower())
            {
                case "required":
                    return "required";
                case "range":
                    return string.Format("ng-minlength=\"{0}\" ng-maxlength=\"{1}\"", val.ValidationParameters["min"], val.ValidationParameters["max"]);
                case "regex":
                    return string.Format("ng-pattern=\"{0}\"", val.ValidationParameters["pattern"]);
                case "length":
                    string lengthRes = "";
                    if (val.ValidationParameters.ContainsKey("min"))
                        lengthRes += string.Format("ng-minlength=\"{0}\" ", val.ValidationParameters["min"]);
                    if (val.ValidationParameters.ContainsKey("max"))
                        lengthRes += string.Format("ng-maxlength=\"{0}\"", val.ValidationParameters["max"]);
                    return lengthRes.TrimEnd();
                default:
                    return string.Format("{0}=\"{1}\"", val.ValidationType, Json.Encode(val.ValidationParameters));
            }
        }


        //public static MvcHtmlString AngularFieldFor<T, R>(
        //    this HtmlHelper<T> htmlHelper, 
        //    Expression<Func<T, R>> selector,
        //    object htmlAttributes = null)
        //{
        //    string name = selector.Name;
        //    //ModelState modelState = htmlHelper.ViewData.ModelState[name];
        //    string expressionText = ExpressionHelper.GetExpressionText(selector);
        //    //ModelErrorCollection modelErrors = (modelState == null) ? null : modelState.Errors;
        //    //ModelError modelError = ((modelErrors == null) || (modelErrors.Count == 0)) ? null : modelErrors[0];
        //    // If there is no error, we want to show a label.  If there is an error,
        //    // we want to show the error message.

        //    string tagText = string.Empty; //labelText;
        //    string tagClass = "form_field_label_normal";
        //    //if ((modelState != null) && (modelError != null))
        //    //{
        //    //    tagText = modelError.ErrorMessage;
        //    //    tagClass = "form_field_label_error";
        //    //}
        //    // Build out the tag
        //    TagBuilder builder = new TagBuilder("span");
        //    builder.MergeAttributes(new RouteValueDictionary(htmlAttributes));
        //    builder.MergeAttribute("class", tagClass);
        //    //builder.MergeAttribute("validationlabelfor", modelName);
        //    builder.SetInnerText(tagText);
        //    return MvcHtmlString.Create(builder.ToString(TagRenderMode.Normal));

        //}//AngularFieldFor
        #endregion

        #region Angular ngMessage div extension
        public static MvcHtmlString NgMessageFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            string formName,
            object htmlAttributes = null)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, new ViewDataDictionary<TModel>());
            var name = ExpressionHelper.GetExpressionText(expression);
            var validations = ModelValidatorProviders.Providers.GetValidators(
                    metadata ?? ModelMetadata.FromStringExpression(name, new ViewDataDictionary()),
                    new ControllerContext()).SelectMany(v => v.GetClientValidationRules()).ToArray();

            return new MvcHtmlString("");
        }//NgMessageFor
        #endregion

    }
}