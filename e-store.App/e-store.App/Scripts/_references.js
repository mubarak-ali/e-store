/// <autosync enabled="true" />
/// <reference path="../ng-app-admin/appadmin.js" />
/// <reference path="../ng-app-admin/controller/category/categoryctrl.js" />
/// <reference path="../ng-app-admin/controller/home/homectrl.js" />
/// <reference path="../ng-app-admin/controller/item/itemctrl.js" />
/// <reference path="../ng-app-admin/controller/item/itemeditctrl.js" />
/// <reference path="../ng-app-admin/controller/subcategory/subcategoryctrl.js" />
/// <reference path="../ng-app-admin/dataserviceadmin.js" />
/// <reference path="../ng-app-user/appuser.js" />
/// <reference path="../ng-app-user/controller/about/aboutctrl.js" />
/// <reference path="../ng-app-user/controller/home/homectrl.js" />
/// <reference path="angular.min.js" />
/// <reference path="angular-animate.min.js" />
/// <reference path="angular-bootstrap.min.js" />
/// <reference path="angular-bootstrap-prettify.min.js" />
/// <reference path="angular-cookies.min.js" />
/// <reference path="angular-loader.min.js" />
/// <reference path="angular-messages.min.js" />
/// <reference path="angular-mocks.js" />
/// <reference path="angular-resource.min.js" />
/// <reference path="angular-route.min.js" />
/// <reference path="angular-sanitize.min.js" />
/// <reference path="angular-scenario.js" />
/// <reference path="bootstrap.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-1.10.2.js" />
/// <reference path="jstd-scenario-adapter.js" />
/// <reference path="jstd-scenario-adapter-config.js" />
/// <reference path="loading-bar.js" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="ng-upload.min.js" />
/// <reference path="respond.js" />
