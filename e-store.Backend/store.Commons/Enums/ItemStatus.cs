﻿public enum ItemStatus
{
    In_Stock = 1,
    Out_Of_Stock = 2,
    Unavailable = 3,
    Discontinued = 4
}