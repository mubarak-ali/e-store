﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e_com.Commons.Dto
{
    public class ItemStatusDto
    {
        public int Id { get; set; }
        public string Status { get; set; }
    }
}
