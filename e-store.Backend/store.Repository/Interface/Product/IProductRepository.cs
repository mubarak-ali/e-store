﻿using DapperExtensions;
using e_com.Repository.Entity;
using System;
using System.Collections.Generic;

namespace e_com.Repository.Interface.Product
{
    public interface IProductRepository : IDisposable
    {
        int InsertCategory(CategoryEntity entity);

        int InsertSubCategory(SubCategoryEntity entity);

        long InsertItem(ItemEntity entity);

        IEnumerable<ItemEntity> GetItem();

        IEnumerable<CategoryEntity> GetCategory();

        IEnumerable<SubCategoryEntity> GetSubCategory();

        IEnumerable<ItemEntity> GetItem(IPredicate predicate);

        IEnumerable<CategoryEntity> GetCategory(IPredicate predicate);

        IEnumerable<SubCategoryEntity> GetSubCategory(IPredicate predicate);

    }
}
