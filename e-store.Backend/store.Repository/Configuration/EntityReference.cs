﻿namespace e_com.Repository.Configuration
{
    public class EntityReference
    {

        #region category Table
        public struct Category
        {
            public static string TABLE_NAME = "category";

            public static string ID = "id";
            public static string NAME = "name";
            public static string DESCRIPTION = "description";
        }
        #endregion

        #region sub_category Table
        public struct SubCategory
        {
            public static string TABLE_NAME = "sub_category";

            public static string ID = "id";
            public static string CATEGORY_ID = "category_id";
            public static string CODE = "code";
            public static string NAME = "name";
            public static string DESCRIPTION = "description";
        }
        #endregion

        #region items Table
        public struct Items
        {
            public static string TABLE_NAME = "item";

            public static string ID = "id";
            public static string CATEGORY_ID = "category_id";
            public static string SUB_CATEGORY_ID = "sub_category_id";
            public static string CODE = "code";
            public static string COLOR = "color";
            public static string DESCRIPTION = "description";
            public static string NAME = "name";
            public static string PRICE = "price";
            public static string SIZE = "size";
            public static string IMAGE = "image";
            public static string STATUS_ID = "item_status_id";
        }
        #endregion

    }
}
