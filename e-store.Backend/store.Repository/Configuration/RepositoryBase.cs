﻿using DapperExtensions.Sql;
using Npgsql;
using System.Configuration;
using System.Data;

namespace e_com.Repository.Configuration
{
    public class RepositoryBase
    {
        private IDbConnection _con;

        public RepositoryBase()
        {
            DapperExtensions.DapperExtensions.SqlDialect = new PostgreSqlDialect();
        }

        protected string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["e-store"].ConnectionString;
        }//GetConnectionString

        protected IDbConnection GetConnection(string connectionString)
        {
            if (_con != null)
            {
                if (_con.State == ConnectionState.Closed)
                {
                    _con.Open();
                    return _con;
                }//if
                else
                {
                    return _con;
                }//else
            }//if
            else
            {
                _con = new NpgsqlConnection(connectionString);
                return _con;
            }//else
        }//GetConnection

        protected IDbConnection GetConnection()
        {
            if (_con != null)
            {
                if (_con.State == ConnectionState.Closed)
                {
                    _con.Open();
                    return _con;
                }//if
                else
                {
                    return _con;
                }//else
            }//if
            else
            {
                _con = new NpgsqlConnection(GetConnectionString());
                return _con;
            }//else
        }//GetConnection

        protected void CloseConnection(IDbConnection connection)
        {
            if (connection != null && connection.State == ConnectionState.Open)
            {
                connection.Close();
                connection.Dispose();
            }//if
        }//CloseConnection
    }
}
