﻿namespace e_com.Repository.Entity
{
    public class CategoryEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        internal bool IsNew
        {
            get
            {
                return Id == default(int);
            }
        }
    }
}
