﻿using e_com.Commons.Dto;
using e_com.Service.Interface;
using System.Collections.Generic;
using System.Web.Http;

namespace e_com.API.Controllers
{
    [RoutePrefix("api/product")]
    public class ProductController : ApiController
    {
        private IProductBL _bll;

        public ProductController(IProductBL bll)
        {
            _bll = bll;
        }

        [Route("item")]
        public IHttpActionResult GetItems()
        {
            var list = _bll.GetItem();
            return Ok(list);
        }

        [Route("category/{id?}")]
        public IHttpActionResult GetCategory(int? id = null)
        {
            IEnumerable<CategoryDto> list = new List<CategoryDto>();
            if (id != null && id > 0)
                list = _bll.GetCategoryById(id ?? default(int));
            else
                list = _bll.GetCategory();

            return Ok(list);
        }

        [Route("subcategory/{id?}")]
        public IHttpActionResult GetSubCategory(int? id = null)
        {
            IEnumerable<SubCategoryDto> list = new List<SubCategoryDto>();
            if (id != null && id > 0)
                list = _bll.GetSubCategoryByCatId(id ?? default(int));
            else
                list = _bll.GetSubCategory();

            return Ok(list);
        }

        [Route("itemStatus")]
        public IHttpActionResult GetItemStatus()
        {
            var statusList = new List<ItemStatusDto>();
            statusList.Add(new ItemStatusDto { Id = (int)ItemStatus.Discontinued, Status = ItemStatus.Discontinued.ToString() });
            statusList.Add(new ItemStatusDto { Id = (int)ItemStatus.In_Stock, Status = ItemStatus.In_Stock.ToString() });
            statusList.Add(new ItemStatusDto { Id = (int)ItemStatus.Out_Of_Stock, Status = ItemStatus.Out_Of_Stock.ToString() });
            statusList.Add(new ItemStatusDto { Id = (int)ItemStatus.Unavailable, Status = ItemStatus.Unavailable.ToString() });
            return Ok(statusList);
        }

        [Route("add/category")]
        [HttpPost]
        public IHttpActionResult AddCategory([FromBody]CategoryDto dto)
        {
            int retValue = _bll.InsertCategory(dto);
            return Ok(retValue);
        }

        [Route("add/subcategory")]
        [HttpPost]
        public IHttpActionResult AddSubCategory([FromBody]SubCategoryDto dto)
        {
            int retValue = _bll.InsertSubCategory(dto);
            return Ok(retValue);
        }

        [Route("add/item")]
        [HttpPost]
        public IHttpActionResult AddItem([FromBody]ItemDto dto)
        {
            long retValue = _bll.InsertItem(dto);
            return Ok(retValue);
        }

    }
}
